#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Tomeu Vizoso and Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from argparse import ArgumentParser, Namespace
import base64
from collections import defaultdict, deque
from copy import deepcopy
from enum import auto, Enum
from expectations_paths import (mesa_expectations_paths,
                                virglrenderer_expectations_paths)
import os
import re
import sys
import time
from typing import Optional, Tuple
from uprevs import uprev_mesa_in_virglrenderer, uprev_piglit_in_mesa

import git
import gitlab
from gitlab import GitlabGetError
from gitlab.v4.objects import (Project, ProjectBranch, ProjectCommit,
                               ProjectJob, ProjectMergeRequest,
                               ProjectPipeline, ProjectPipelineJob)
from pprint import pformat
from tenacity import retry, stop_after_attempt, wait_exponential

# GLOBAL constants and objects

GITLAB_TOKEN: str = ""
gl: gitlab.Gitlab

TARGET_PROJECT_PATH: str
FORK_PROJECT_PATH: str
DEP_PROJECT_PATH: str

LOCAL_CLONE: str
UPREV_TMP_BRANCH: str
UPREV_EXPECTATION_CHANGES_BRANCH: str
UPREV_BRANCH: str

MR_TITLE: str
EXPECTATIONS_UPDATED_SUFFIX: str
ISSUE_TITLE: str

target_project: Project
fork_project: Project
dep_project: Project
working_project: Project
template_project: Project

IN_PRODUCTION: bool

LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE: int = 10
MAX_LINES_PER_CATEGORY_IN_NOTE: int = 100

CHECKER_SLEEP_TIME: int = 10


class UpdateRevision(Enum):
    mesa_in_virglrenderer = auto()
    piglit_in_mesa = auto()


uprev_pair: UpdateRevision


def prepare_environment(
        default_target_namespace: str = 'virgl',
        default_target: str = 'virglrenderer',
        default_dep_namespace: str = 'mesa',
        default_dep: str = 'mesa'
        ) -> None:
    """
    Fundamental method to gather information from the environment and prepare
    the objects and references that will be used by the tool.

    By 'target' we mean the project to where the uprev is to be committed.
    By 'fork' we mean the user repo where the work is made and from where the
    merge request will be created to the target.
    By 'dep' we mean the project whose reference will be updated in the target.
    :return:
    """
    global GITLAB_TOKEN
    GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
    if GITLAB_TOKEN is None:
        try:
            with open(os.path.expanduser('~/.gitlab-token')) as file_descriptor:
                GITLAB_TOKEN = file_descriptor.read().strip()
        except FileNotFoundError:
            raise AssertionError(
                f"Use the GITLAB_TOKEN environment variable or "
                f"the ~/.gitlab-token to provide this information")
    global gl
    gl = gitlab.Gitlab(url='https://gitlab.freedesktop.org',
                       private_token=GITLAB_TOKEN)
    gl.auth()

    global TARGET_PROJECT_PATH
    global FORK_PROJECT_PATH
    global DEP_PROJECT_PATH
    TARGET_PROJECT_PATH = os.environ.get(
        'TARGET_PROJECT_PATH', f'{default_target_namespace}/{default_target}')
    target_name = TARGET_PROJECT_PATH.split('/')[1]
    FORK_PROJECT_PATH = os.environ.get(
        'FORK_PROJECT_PATH', f'{gl.user.username}/{target_name}')
    DEP_PROJECT_PATH = os.environ.get(
        'DEP_PROJECT_PATH', f'{default_dep_namespace}/{default_dep}')
    dep_name = DEP_PROJECT_PATH.split('/')[1]
    global uprev_pair
    try:
        uprev_name = f'{dep_name}_in_{target_name}'
        uprev_pair = UpdateRevision[uprev_name]
    except AttributeError:
        raise NotImplementedError(f"Not yet implemented the uprev of "
                                  f"{dep_name} in {target_name}")

    global LOCAL_CLONE
    global UPREV_BRANCH
    global UPREV_TMP_BRANCH
    global UPREV_EXPECTATION_CHANGES_BRANCH
    LOCAL_CLONE = f"./tmp_{target_name}.git"
    # Branch to which we push if there are only UnexpectedPasses
    UPREV_BRANCH = f"uprev-{dep_name}"
    # Branch to which we first push to get the initial results
    UPREV_TMP_BRANCH = f"{UPREV_BRANCH}-tmp"
    # Branch to which we push if there are possible regressions
    UPREV_EXPECTATION_CHANGES_BRANCH = f"{UPREV_BRANCH}-expectation-changes"

    global MR_TITLE
    global EXPECTATIONS_UPDATED_SUFFIX
    global ISSUE_TITLE
    MR_TITLE = f'ci: Uprev {dep_name.title()}'
    EXPECTATIONS_UPDATED_SUFFIX = 'with expectation changes'
    ISSUE_TITLE = f'{MR_TITLE} failed'

    global target_project
    global fork_project
    global dep_project
    global working_project
    global template_project
    target_project = gl.projects.get(TARGET_PROJECT_PATH)
    fork_project = gl.projects.get(FORK_PROJECT_PATH)
    dep_project = gl.projects.get(DEP_PROJECT_PATH)
    working_project = fork_project
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        template_project = dep_project  # project where the template is defined
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        template_project = target_project

    production_envvar = os.environ.get('PRODUCTION', 'False').lower()
    if production_envvar not in ['0', '1', 'false', 'true']:
        raise AssertionError(
            f"Unrecognized value in PRODUCTION environment variable "
            f"(value '{production_envvar}')")
    global IN_PRODUCTION
    IN_PRODUCTION = production_envvar in ('1', 'true')
    if not IN_PRODUCTION:
        UPREV_EXPECTATION_CHANGES_BRANCH = \
            f"{UPREV_EXPECTATION_CHANGES_BRANCH}-testonly"
        UPREV_BRANCH = f"{UPREV_BRANCH}-testonly"


def get_candidate_for_uprev() -> Tuple[int, str]:
    """
    First get pipelines in the project where it has to update the revision.
    Then, chose the first one that satisfy specific conditions.
    :return: pipeline id and commit hash
    """
    pipelines = __get_pipelines()
    pipeline = __select_pipeline(pipelines)
    if not pipeline:
        raise LookupError("No uprev candidate found")
    return pipeline.id, pipeline.sha


def __get_pipelines(
        status: str = 'success',
        username: str = 'marge-bot'
        ) -> gitlab.base.RESTObjectList:
    """
    To update the revision, the first requisite is a pipeline that:
    1. It has to have succeeded,
    2. It has to be merged by marge
    :return: iterator over pipelines
    """
    return dep_project.pipelines.list(status=status, username=username,
                                      ordered_by='created_at', sort='desc',
                                      iterator=True)


def __select_pipeline(
        pipelines: gitlab.base.RESTObjectList
        ) -> ProjectPipeline:
    """
    Specific condition that the pipeline candidate must satisty. In the case of
    mesa for virglrenderer it is necessary (because it uses some artifacts from
    the merge pipeline) to have the 'debian-testing' job.
    :return: the pipeline to use in the uprev.
    """
    for pipeline in pipelines:
        if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
            debian_testing_job = [job
                                  for job in pipeline.jobs.list(iterator=True)
                                  if job.name == 'debian-testing']
            if debian_testing_job:
                return pipeline
        else:
            return pipeline


def get_templates_commit(
        revision: str,
        filename: str = ".gitlab-ci.yml"
        ) -> str:
    """
    Find in a file of a gitlab project the definition of the
    MESA_TEMPLATES_COMMIT hash to be used.
    :param revision: define with commit, tag, branch use to read the file
    :param filename: file of the project where the variable is defined
    :return: hash of ci-templates to use in the uprev
    """
    file = template_project.files.get(filename, ref=revision)
    file_content = base64.b64decode(file.content).decode("utf-8")
    for line in file_content.split('\n'):
        # sample line '    MESA_TEMPLATES_COMMIT: &ci-templates-commit <hash>'
        # and we are interested in the hash at the end
        pattern = r'MESA_TEMPLATES_COMMIT:.* ([0-9a-fA-F].*)$'
        if search_result := re.search(pattern, line):
            return search_result.group(1)


def create_branch(
        pipeline_id: int,
        revision: str,
        templates_commit: str,
        default_branch: str
        ) -> git.Repo:
    """
    With the uprev information, clone locally the target project and prepare
    the temporal working branch from the target's default branch. Then do the
    uprev ifself and commit it.
    :param pipeline_id: reference pipeline of the dep project to uprev
    :param revision: dep project revision to uprev
    :param templates_commit: ci-templates commit to uprev
    :param default_branch: target default branch from where create the working
    branch
    :return: git repository object
    """
    if os.path.exists(LOCAL_CLONE):
        print(f"Reusing repo in {LOCAL_CLONE}")
        repo = git.Repo(LOCAL_CLONE)
        repo.remote("origin").update()
        repo.remote("fork").update()

        if UPREV_TMP_BRANCH in repo.heads:
            print(f"Deleting existing branch")
            repo.heads[default_branch].checkout(force=True)
            repo.delete_head(UPREV_TMP_BRANCH, force=True)
    else:
        print(f"Cloning repo")
        repo_url = f"https://gitlab.freedesktop.org/{TARGET_PROJECT_PATH}.git"
        repo = git.Repo.clone_from(repo_url, LOCAL_CLONE,
                                   branch=default_branch)
        repo.config_writer().set_value("user", "name",
                                       "Collabora's Gfx CI Team").release()
        repo.config_writer().set_value("user", "email", gl.user.email).release()
        fork_url = f"https://{gl.user.username}:{GITLAB_TOKEN}@" \
                   f"gitlab.freedesktop.org/{FORK_PROJECT_PATH}.git"
        repo.create_remote("fork", url=fork_url)
    __clean_previous_tmp_branch()

    repo.heads[default_branch].checkout(force=True, b=UPREV_TMP_BRANCH)
    print(f"start working from the commit with SHA {repo.head.commit.hexsha} "
          f"{target_project.commits.get(repo.head.commit.hexsha).web_url}")

    os.chdir(repo.working_dir)
    try:
        if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
            uprev_mesa_in_virglrenderer(repo, pipeline_id, revision,
                                        templates_commit)
        elif uprev_pair == UpdateRevision.piglit_in_mesa:
            uprev_piglit_in_mesa(repo, revision)
        else:
            raise NotImplementedError(f"Not yet implemented the uprev of "
                                      f"{uprev_pair.name}")
        print(f"Created commit with SHA {repo.head.commit.hexsha}")
    finally:
        os.chdir('..')

    return repo


def __clean_previous_tmp_branch() -> None:
    """
    The temporary branch could still be in the fork repo because there is
    another uprev procedure in progress or because one failed lefting it orphan.
    So, when it is orphan a newer uprev procedure could fail and requires it
    to be cleaned.
    :return:
    """
    if (branch := __get_branch(UPREV_TMP_BRANCH)) is None:
        return
    print(f"The branch {UPREV_TMP_BRANCH} exists in the fork")
    __wait_for_running_pipelines_in_branch(branch)
    print(f"Remove orphan {UPREV_TMP_BRANCH} branch")
    branch.delete()


def __get_branch(name: str) -> Optional[ProjectBranch]:
    """
    Get the branch gitlab object or return None hiding any exception
    :param name: string with branch name
    :return: The project branch object or None
    """
    try:
        return fork_project.branches.get(name)
    except GitlabGetError:
        return


def __wait_for_running_pipelines_in_branch(branch: ProjectBranch) -> None:
    """
    When a branch have pipelines running, wait until they finish. They could be
    part of a procedure that could fail if we intervene.
    :param branch: branch that could have pipelines running.
    :return:
    """
    while active_pipelines := [
            pipeline.id
            for pipeline in fork_project.pipelines.list(sha=branch.commit['id'],
                                                        iterator=True)
            if pipeline.status == 'running']:
        if len(active_pipelines) > 0:
            print(f"Branch {UPREV_TMP_BRANCH} has {len(active_pipelines)} "
                  f"pipeline(s) running. Waiting them to finish... "
                  f"({active_pipelines})")
            time.sleep(CHECKER_SLEEP_TIME)


def push(
        repo: git.Repo
        ) -> ProjectPipeline:
    """
    Push the commits in the local repository to the fork and wait until the
    last commit has a pipeline created.
    :param repo: local clone
    :return: commit pipeline
    """
    remote = repo.remote("fork")
    remote.push(force=True)
    return __wait_pipeline_creation(repo.head.commit.hexsha)


def __wait_pipeline_creation(
        commit_hash: str,
        source: str = None
        ) -> ProjectPipeline:
    """
    For a pushed commit to the fork, Peaceful wait for the pipeline creation.
    We can use the 'source' parameter to fine-tune the pipeline wanted.
    :param commit_hash: string
    :param source: string
    :return: pipeline
    """
    print(f"Waiting for pipeline to be created for {commit_hash} "
          f"{__get_commit(commit_hash).web_url}")
    while True:
        pipelines = working_project.pipelines.list(sha=commit_hash,
                                                   source=source)
        if pipelines:
            return pipelines[0]
        time.sleep(CHECKER_SLEEP_TIME)


@retry(stop=stop_after_attempt(10),
       wait=wait_exponential(multiplier=1, max=180))
# retry waiting 1, 2, 4, 8, 16, 32, 64, 128, 180... > 10 minutes
def __get_commit(commit_hash: str) -> ProjectCommit:
    """
    Get a commit object from the working project. If there is an exception, log
    it and reraise. It has also a decorator to use tenacity to retry.
    :param commit_hash: identifier string of the commit1
    :return: object in gitlab representing a commit.
    """
    try:
        return working_project.commits.get(commit_hash)
    except Exception as exception:
        print(f"Couldn't get the commit {commit_hash} "
              f"due to {type(exception)}: {exception}")
        raise exception


def run(
        pipeline: ProjectPipeline
        ) -> dict:
    """
    Trigger the manual jobs in the pipeline (waiting, if necessary, to the
    complete creation of the pipeline in the server) and wait until the pipeline
    finishes.
    Then collect the artifacts. Give it a second chance if any jobs still need
    to produce artifacts. And also, run a second time the ones that made
    failures artifacts.
    :param pipeline: git lab object
    :param project: when pipeline to run is in a different project than the fork
    :return: failures dict
    """
    print(f"Waiting for pipeline to be ready {pipeline.web_url}")
    while pipeline.status != 'manual':
        time.sleep(CHECKER_SLEEP_TIME)
        pipeline = working_project.pipelines.get(pipeline.id)
    print(f"Triggering jobs for pipeline {pipeline.web_url}")
    for job in pipeline.jobs.list(iterator=True):
        if __pipeline_trigger_job_condition(job):
            pjob = working_project.jobs.get(job.id, lazy=True)
            pjob.play()
    pipeline = __wait_pipeline_start(pipeline)
    pipeline = __wait_pipeline_finished(pipeline)
    return __post_process_pipeline(pipeline)


def __pipeline_trigger_job_condition(
        job: ProjectPipelineJob
        ) -> bool:
    """
    Determines whether a pipeline job should be triggered based on certain
    conditions.
    :param job: The pipeline job to evaluate.
    :return: Boolean indicating whether the job should be triggered.
    """
    if job.status == 'manual':
        return True
    return False


def __post_process_pipeline(
        pipeline: ProjectPipeline
        ) -> dict:
    """
    Check if there are results in the pipeline jobs. When some job has finished
    without artifacts, give it a second chance to make them.
    To detect flakes in the tests, the failed jobs are retried to see if they
    produce the same outcome.
    :param pipeline: pipeline to post process
    :return: dictionary with the failures found
    """
    failures, artifacts, jobs_without_artifacts = collate_results(pipeline)
    if jobs_without_artifacts:
        pipeline, artifacts = \
            __retry_jobs_without_artifacts(pipeline, artifacts,
                                           jobs_without_artifacts)
    if artifacts:
        _, failures, _ = __retry_flake_candidates(pipeline, failures, artifacts)

    return failures


def __wait_pipeline_start(pipeline: ProjectPipeline) -> ProjectPipeline:
    """
    Once a manual jobs are triggered in a pipeline, there is a (usually short
    but non-negligible) time
    :param pipeline: ProjectPipeline
    :return: ProjectPipeline
    """
    print(f"Wait for pipeline {pipeline.web_url} to start "
          f"(status={pipeline.status})")
    while True:
        status_changed, pipeline = __refresh_pipeline_status(pipeline)
        if status_changed and pipeline.status in ["running", "failed"]:
            print(f"Pipeline {pipeline.web_url} in {pipeline.status} status")
            return pipeline
        time.sleep(CHECKER_SLEEP_TIME)


def __wait_pipeline_finished(pipeline: ProjectPipeline) -> ProjectPipeline:
    """
    Periodically check if the pipeline has finished.
    :param pipeline: ProjectPipeline
    :return: ProjectPipeline
    """
    if pipeline.status in ["failed"]:
        return pipeline
    print(f"Wait for pipeline {pipeline.web_url} to finish "
          f"(status={pipeline.status})")
    while True:
        status_changed, pipeline = __refresh_pipeline_status(pipeline)
        if status_changed and \
                pipeline.status not in ["created", "waiting_for_resource",
                                        "preparing", "pending", "running"]:
            print(f"Pipeline {pipeline.web_url} finished "
                  f"(status={pipeline.status})")
            return pipeline
        time.sleep(CHECKER_SLEEP_TIME)


def __refresh_pipeline_status(
        pipeline: ProjectPipeline
        ) -> Tuple[bool, ProjectPipeline]:
    """
    Check if the pipeline status has changed since the last object refresh.
    :param pipeline: ProjectPipeline
    :return: bool, ProjectPipeline
    """
    previous_pipeline_status = pipeline.status
    pipeline = working_project.pipelines.get(pipeline.id)
    if previous_pipeline_status != pipeline.status:
        print(f"Pipeline {pipeline.id} status change: "
              f"{previous_pipeline_status} -> {pipeline.status}")
        return True, pipeline
    return False, pipeline


def __retry_jobs_without_artifacts(
        pipeline: ProjectPipeline,
        artifacts: dict,
        jobs_without_artifacts: list
    ) -> Tuple[ProjectPipeline, dict]:
    """
    if there are jobs without artifacts, lets give them another try
    :return:
    """
    print(f"Some jobs didn't produce artifacts")
    for job in jobs_without_artifacts:
        print(f"Retry '{job.name}' for artifacts build.")
        pjob = working_project.jobs.get(job.id, lazy=True)
        pjob.retry()
    pipeline = __wait_pipeline_start(pipeline)
    pipeline = __wait_pipeline_finished(pipeline)
    # collect results again, but:
    # - remembering artefacts already downloaded to avoid overwriting
    _, artifacts, jobs_without_artifacts = collate_results(pipeline,
                                                           artifacts=artifacts)
    if len(jobs_without_artifacts) != 0:
        job_names = [job.name for job in jobs_without_artifacts]
        __uprev_failed_issue(pipeline,
                             f"These jobs failed to produce artifacts for "
                             f"a second time: {', '.join(job_names)}")
        sys.exit(0)
    return pipeline, artifacts


def __retry_flake_candidates(
        pipeline: ProjectPipeline,
        failures: dict,
        artifacts: dict
    ) -> Tuple[ProjectPipeline, dict, dict]:
    """
    Do one retry in the jobs that may have failed to
    :param pipeline: ProjectPipeline
    :return: ProjectPipeline
    """
    for job in pipeline.jobs.list(iterator=True):
        if job.status == 'failed':
            print(f"Retry '{job.name}' to discard flakes")
            pjob = working_project.jobs.get(job.id, lazy=True)
            pjob.retry()
    pipeline = __wait_pipeline_start(pipeline)
    pipeline = __wait_pipeline_finished(pipeline)
    # collect results again, but:
    # - remembering failures to collect together new results
    # - remembering artefacts already downloaded to avoid duplication
    _, _, jobs_without_artifacts = collate_results(
        pipeline, failures=failures, artifacts=artifacts)
    if len(jobs_without_artifacts) != 0:
        pipeline, artifacts = __retry_jobs_without_artifacts(
            pipeline, artifacts, jobs_without_artifacts)
    failures, artifacts, _ = collate_results(
        pipeline, failures=failures, artifacts=artifacts, recollect=True)
    return pipeline, failures, artifacts


def __uprev_failed_issue(
        pipeline: ProjectPipeline,
        reason: str,
        ) -> None:
    """
    When the uprev produces jobs that doesn't produce artifacts, it means there
    is something deeper about the uprev that cannot be solved with an
    expectations update. Further information has to be provided to the
    developers to know about the situation.
    """
    job_id = os.getenv('CI_JOB_ID')
    if job_id is not None:
        uprev_project = gl.projects.get(os.getenv('CI_PROJECT_ID'))
        uprev_job = uprev_project.jobs.get(job_id)
        description = [
            f"This information comes from the execution of ci-uprev in job "
            f"[{uprev_job.id}]({uprev_job.web_url}).\n\n"]
    else:
        description = [
            f"This information comes from a local execution of ci-uprev.\n\n"]
    description.append(f"Related pipeline: "
                       f"[{pipeline.id}]({pipeline.web_url}).\n")
    description.append(f"ci-uprev reported:\n\n\t{reason}\n")
    uprev_commit = working_project.commits.get(pipeline.sha)
    start_commt = working_project.commits.get(uprev_commit.parent_ids[0])
    dep_commit = dep_project.commits.get(uprev_commit.title.split()[-1])
    description.append(
        f"This happened while doing an update revision of {dep_project.name} "
        f"[revision {dep_commit.short_id}]({dep_commit.web_url}) in "
        f"{target_project.name} [revision "
        f"{start_commt.short_id}]({start_commt.web_url}) with the uprev "
        f"[commit {uprev_commit.short_id}]({uprev_commit.web_url}).")
    for issue in target_project.issues.list(state='opened', iterator=True):
        if issue.title == ISSUE_TITLE:
            issue.notes.create({"body": ''.join(description)})
            break
    else:  # executed when break statement hasn't been called
        if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
            labels = ['ci']
        elif uprev_pair == UpdateRevision.piglit_in_mesa:
            labels = ['CI']
        else:
            labels = []
        issue = target_project.issues.create(
            {'title': ISSUE_TITLE, 'description': ''.join(description),
             'labels': labels})
    print(f"Created an issue with the runtime warning {issue.web_url}")


def collate_results(
        pipeline: ProjectPipeline,
        failures: dict = None,
        artifacts: dict = None,
        recollect: bool = False
        ) -> Tuple[dict, dict, list]:
    """
    Find and collect the artifacts of the jobs. Classify them in a dictionary
    or, if there isn't, collect those jobs in a list.
    :param pipeline: ProjectPipeline
    :param failures: dict
    :param artifacts: dict
    :param recollect: bool
    :return: failures, artifacts, without_artifacts
    """
    print(f"Looking for failed jobs in pipeline {pipeline.web_url}")

    def defaultdictdeque() -> defaultdict:
        return defaultdict(deque)

    if artifacts is None:
        artifacts = dict()
    if failures is None:
        # failures dict has as keys the tripplet (test_suite, backend, api)
        #  as value it has a nested dict where the key is the test_name and
        #  the values are deque of ens states of the tests in different reties.
        failures = defaultdict(defaultdictdeque)
    else:
        failures = deepcopy(failures)  # propagate will be decided by return
    without_artifacts = []

    for job in pipeline.jobs.list(iterator=True):
        job = working_project.jobs.get(job.id)

        if job.status != 'failed':
            continue

        if __is_fundamental_job(job):
            print(f"Something very bad happened: "
                  f"a build or sanity test job failed! "
                  f"({job.name} job in {job.stage} stage)")
            sys.exit(0)

        job_classification_key = __classify_the_job(job)

        if __download_artifacts(job, artifacts):
            results = artifacts[job.name]
        else:
            without_artifacts.append(job)
            continue

        __collate_failures(results, failures[job_classification_key], recollect)

    if recollect:
        __implicit_results_become_explicit(failures)

    return failures, artifacts, without_artifacts


def __is_fundamental_job(
        job: ProjectJob
        ) -> bool:
    """
    Different uprev would have different definitions to classify their jobs
    between what's a fundamental job in the pipeline. They are jobs that create
    things that are required in other jobs.
    :param job: ProjectJob
    :return: bool
    """
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        return job.stage in ['build', 'sanity test']
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        return job.stage in ['sanity', 'container', 'build-x86_64',
                             'build-misc', 'lint']


def __classify_the_job(
        job: ProjectJob
        ) -> str | Tuple:
    """
    Different uprev would have different ways to classify jobs. So we would
    specialize this method depending on the pair to uprev.
    :param job: ProjectJob
    :return: key
    """
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        return __unshard_job_name(job.name)
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        return __unshard_job_name(job.name), job.stage


def __download_artifacts(
        job: ProjectJob,
        artifacts: dict
        ) -> bool:
    """
    For a given job, find if it has left artifacts (there could be more than
    one location to look at) to be downloaded. If so, collect it to be
    processed next. On the contrary, report returning false.
    :return:
    """
    def failures_path_generator() -> str:
        yield 'results/failures.csv'
        yield f'results/{__unshard_job_name(job.name)}/failures.csv'

    failures_path = ""
    try:
        if job.name not in artifacts:
            artifact = None
            for failures_path in failures_path_generator():
                try:
                    artifact = job.artifact(failures_path)
                except gitlab.exceptions.GitlabGetError as inner_exception:
                    code = inner_exception.response_code
                    if code == 404:
                        # go to check the next path with the generator
                        continue
                    raise inner_exception
                print(f"Downloaded artifacts for '{job.name}' "
                      f"from '{failures_path}' at {job.web_url}")
            if artifact is None:
                raise FileNotFoundError(f"No artifacts found")
            artifacts[job.name] = artifact
        return True
    except Exception as outer_exception:
        print(f"An exception ({type(outer_exception)}) occurred "
              f"when downloading results for '{job.name}' "
              f"from '{failures_path}' at {job.web_url}: "
              f"{outer_exception}!")
        return False


def __collate_failures(
        results: bytes,
        job_failures: dict,
        recollect: bool
        ) -> None:
    """
    Process the lines in the results downloaded to insert the data to the
    failures structure of a given job.
    :param results: content of the artifact downloaded
    :param job_failures: key-value of the test and the results it has had
    :param recollect: if this happened after a failed jobs retry
    :return:
    """
    for line in results.decode('utf-8').split('\n'):
        if not line.strip():
            continue
        test_name, result = line.split(',')
        # detect flakiness: collect the current execution results
        job_failures[test_name].append(result)
        if recollect and len(job_failures[test_name]) == 1:
            # As we only collect when fail is reported or unexpected pass,
            #  complete the information
            failures_item = job_failures[test_name]
            if result == "UnexpectedPass":
                failures_item.appendleft("ExpectedFail")
            else:
                failures_item.appendleft("Pass")


def __implicit_results_become_explicit(
        failures: dict
        ) -> None:
    """
    review and complete the list of pairs with the implicit pass
    :param failures: dictionary with the results already collected
    :return: dictionary with the implicit results made explicit
    """
    for test_dict in failures.values():
        for results_deque in test_dict.values():
            if len(results_deque) == 1:
                if results_deque[0] == "UnexpectedPass":
                    results_deque.append("ExpectedFail")
                else:
                    results_deque.append("Pass")


def __unshard_job_name(
        name: str
        ) -> str:
    """
    Remove the suffix on the sharded jobs to have a single name for all of them.
    :param name: string
    :return: original name before sharding
    """
    search = re.search(r"(.*) [1-9]/[1-9]$", name)
    if search:  # if it has a " n/m" at the end, remove it.
        return search.group(1)
    return name


def update_branch(
        repo: git.Repo,
        failures: dict
        ) -> None:
    """
    Using the local clone of the repository, review the failures found
    (if there are any) to update the expectations (if required).
    :param repo: working repository
    :param failures:
    :return:
    """
    os.chdir(repo.working_dir)
    try:
        index = repo.index  # This is expensive, reuse the index object
        update_expectations = False
        for job_key, test_dict in failures.items():
            expectations = __get_expectations_path_and_file(job_key)
            if expectations is None:
                continue
            fails, flakes = __get_expectations_contents(*expectations)
            existing_fails = __fail_tests_and_their_causes(fails['content'])

            fixed_tests, update_expectations = __include_new_fails_and_flakes(
                job_key, test_dict, flakes['content'], fails['content'],
                existing_fails, fails['name'])

            __remove_fixed_tests(fixed_tests, fails['content'])

            for file_name, content in [(flakes['name'], flakes['content']),
                                       (fails['name'], fails['content'])]:
                __patch_content(index, file_name, content)

        if update_expectations:
            repo.git.commit("--amend", "--no-edit")
            print(f"Amended commit with new SHA {repo.head.commit.hexsha}\n"
                  f"The pipeline requires manual intervention. "
                  f"So, using the {UPREV_EXPECTATION_CHANGES_BRANCH} branch.")
            __rename_active_branch(repo, UPREV_EXPECTATION_CHANGES_BRANCH)
        else:
            __rename_active_branch(repo, UPREV_BRANCH)
    finally:
        os.chdir('..')


def __rename_active_branch(repo: git.Repo, new_name: str) -> None:
    """
    To complete the rename action, it is necessary to do it in two steps
    because the gitpython operation doesn't remove the old one in the remote.
    The new branch is only local now. The step to push it comes next by call
    push_to_merge_request().
    :param repo: working repository
    :param new_name: destination name for the temporary branch
    :return:
    """
    repo.active_branch.rename(new_name, force=True)
    repo.remotes.fork.push(refspec=f":{UPREV_TMP_BRANCH}")


def __get_expectations_path_and_file(
        job_key: str
        ) -> Optional[Tuple]:
    """
    Find in the structure where are located the expectation files for a given
    job. So it returns two strings. The first is with the path within the
    project. The second is with the prefix of the files that store the fails,
    flakes, and skips.
    :param job_key: string
    :return: a pair of strings
    """
    try:
        if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
            expectations_paths = virglrenderer_expectations_paths
        elif uprev_pair == UpdateRevision.piglit_in_mesa:
            expectations_paths = mesa_expectations_paths
        else:
            raise NotImplementedError(f"Not yet implemented the expectations "
                                      f"update for {target_project.name}")
        path = expectations_paths[job_key]['path']
        files = expectations_paths[job_key]['files']
        print(f"For {job_key!r} the path is {path!r} and files {files!r}")
        return path, files
    except KeyError:
        print(f"The tool is not yet managing expectations for {job_key}")
        return


def __get_expectations_contents(
        path: str,
        prefix: str
        ) -> Tuple[dict, dict]:
    """
    Get the contents of the files that could be necessary to update the
    expectations.
    :param path: string
    :param prefix: string
    :return: two dicts with the name and content of the files
    """
    fails_file_name = f"{path}/{prefix}-fails.txt"
    fails = {'name': fails_file_name,
             'content': __get_file_content(fails_file_name)}
    flakes_file_name = f"{path}/{prefix}-flakes.txt"
    flakes = {'name': flakes_file_name,
              'content': __get_file_content(flakes_file_name)}
    return fails, flakes


def __fail_tests_and_their_causes(
        fails_contents: list
        ) -> dict:
    """
    From the list of lines in the fails file, convert it to a dictionary where
    the key is the test name, and the value is the test results and the line.
    We can later check if the test has failed for the same reason or another.
    :param fails_contents:
    :return:
    """
    dct = {}
    for i, line in enumerate(fails_contents, start=1):
        if line.startswith('#') or line.count(',') == 0:
            continue  # ignore these lines
        test_name, test_result = line.strip().rsplit(',', 1)
        dct[test_name] = {'result': test_result, 'line': i}
    return dct


def __include_new_fails_and_flakes(
        job_key: str,
        tests: dict,
        flakes_content: list,
        fails_content: list,
        existing_fails: dict,
        fails_file_name: str
        ) -> Tuple[list, bool]:
    """
    Prepare the content for the fails and flake files with the newer
    information from the uprev pipeline execution.
    :rtype: object
    :param job_key: identifier
    :param tests: list
    :param flakes_content: file content
    :param fails_content: file content
    :param existing_fails: dict with the known fails and their result
    :param fails_file_name: file name to include in the report
    :return: list of test that are fix with the uprev, flag about if it has
             made an expectations update
    """
    expectations_change = False
    fixed_tests = []
    for test_name, test_results in tests.items():
        # remove duplicated elements
        test_results_unique = set(test_results)
        assert len(
            test_results_unique) > 0, \
            "Failed, tests should have at least one result"
        if len(test_results_unique) != 1:
            print(f"Flaky test detected in {job_key} "
                  f"{test_name=} with results {list(test_results)}")
            flakes_content += [f"{test_name}\n"]
            expectations_change = True
        elif test_results[0] == "UnexpectedPass":
            print(f"UnexpectedPass test detected in {job_key} "
                  f"{test_name=}")
            fixed_tests += [test_name]
            expectations_change = True
        else:
            if test_name in existing_fails.keys():
                previous_test_result = existing_fails[test_name]['result']
                line = existing_fails[test_name]['line']
                if test_results[0] == previous_test_result:
                    print(f"Failed test detected in {job_key} {test_name=} "
                          f"with results '{test_results[0]}' was already in "
                          f"{fails_file_name}:{line} with the same cause.")
                    # This shouldn't happen, but log it just in case.
                else:
                    print(f"Failed test detected in {job_key} {test_name=} "
                          f"with results '{test_results[0]}' was already in "
                          f"{fails_file_name}:{line} with a different cause "
                          f"(was '{previous_test_result}').")
                    fails_content[line-1] = \
                        f"{test_name},{test_results[0]}\n"
                    expectations_change = True
            else:
                print(f"Failed test detected in {job_key} {test_name=} "
                      f"with results {test_results[0]}")
                fails_content += [f"{test_name},{test_results[0]}\n"]
                expectations_change = True
    return fixed_tests, expectations_change


def __remove_fixed_tests(
        fixed_tests: list,
        fails_content: list
        ) -> None:
    """
    Review the list of tests that are fixed with the uprev to remove them from
    the list of tests that it was known they fail.
    :param fixed_tests:
    :param fails_content:
    :return:
    """
    for test_name in fixed_tests:
        if f"{test_name},Fail\n" in fails_content:
            fails_content.remove(f"{test_name},Fail\n")
        if f"{test_name},Crash\n" in fails_content:
            fails_content.remove(f"{test_name},Crash\n")


def __patch_content(
        index: git.index.base.IndexFile,
        file_name: str,
        contents: list
        ) -> None:
    """
    Once the file's content is ready, write in the file and add it to the git
    repo to prepare it to be committed.
    :param index: gitpython object to control the commit
    :param file_name: string
    :param contents: list of lines
    :return:
    """
    with open(file_name, "wt") as file_descriptor:
        file_descriptor.writelines(contents)
    print(f"Patched file {file_name}")
    index.add(file_name)


def __get_file_content(
        file_name: str
        ) -> list:
    """
    Return the lines in a file as a list.
    :param file_name:
    :return:
    """
    if os.path.exists(file_name):
        with open(file_name, "rt") as file_descriptor:
            return file_descriptor.readlines()
    else:
        return list()


def push_to_merge_request(
        repo: git.Repo,
        failures: dict,
        pipeline: ProjectPipeline
        ) -> None:
    """
    Once we have a viable candidate to uprev, generate or append to an existing
    merge request the proposal. It can be with or without an update on the
    expectations. Once the merge request has a pipeline to verify this proposal,
    the tool triggers it.
    :param repo: git.Repo
    :param failures: dict
    :param pipeline: ProjectPipeline
    :return: None
    """
    push(repo)
    regressions, flakes, unexpectedpass = __get_failures_by_category(failures)
    note_comments = __prepare_comments(pipeline,
                                       regressions, flakes, unexpectedpass)
    expectations_updated = bool(regressions or flakes or unexpectedpass)
    merge_request = __get_merge_request(expectations_updated)
    __append_note_to_merge_request(merge_request, note_comments)
    __run_merge_request_pipeline(repo.head.commit.hexsha)


def __get_failures_by_category(
        failures: dict
        ) -> Tuple[dict, dict, dict]:
    """
    From the collection of failures, classify them by category if there are
    regressions or flakes.
    :param failures: dict
    :return: Tuple[regressions, flakes, unexpectedpass]
    """
    regressions_per_category = defaultdict(dict)
    flake_per_category = defaultdict(dict)
    unexpectedpass_per_category = defaultdict(dict)
    for job_name, test_dict in failures.items():
        for test_name, results in test_dict.items():
            results_unique = set(results)
            if len(results_unique) != 1:
                category = f"Unreliable tests on {job_name}:\n"
                flake_per_category[category][test_name] = list(results)
            elif results[0] != "UnexpectedPass":
                category = f"Possible regressions on {job_name}:\n"
                regressions_per_category[category][test_name] = results[0]
            else:
                assert len(results_unique) == 1
                assert results[0] == "UnexpectedPass"
                category = f"Fix test with the uprev on {job_name}:\n"
                unexpectedpass_per_category[category][test_name] = results[0]
    return (regressions_per_category, flake_per_category,
            unexpectedpass_per_category)


def __prepare_comments(
        pipeline: ProjectPipeline,
        regressions_per_category: dict,
        flake_per_category: dict,
        unexpectedpass_per_category: dict
        ) -> Tuple[list, list, list]:
    """
    With the information available, prepare the list of comments to be added
    in a note in the merge request.
    :param pipeline: ProjectPipeline
    :param regressions_per_category: dict
    :param flake_per_category: dict
    :param unexpectedpass_per_category: dict
    :return: list
    """
    comments = \
        __source_comment(pipeline), \
        __generate_comment(regressions_per_category, False), \
        __generate_comment(flake_per_category, True), \
        __generate_comment(unexpectedpass_per_category, False)
    return comments


def __get_merge_request(
        expectations_updated: bool
        ) -> ProjectMergeRequest:
    """
    Check if a merge request is already in progress or create a newer one.
    :param expectations_updated: bool
    :return: ProjectMergeRequest
    """
    merge_request = None
    mr_title = __get_merge_request_title(expectations_updated)

    for candidate in \
            target_project.mergerequests.list(state='opened', iterator=True):
        if mr_title == candidate.title and \
                candidate.author["username"] == gl.user.username:
            merge_request = candidate
            break
    if merge_request is None:
        if expectations_updated:
            branch = UPREV_EXPECTATION_CHANGES_BRANCH
        else:
            branch = UPREV_BRANCH
        if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
            labels = ['ci']
        elif uprev_pair == UpdateRevision.piglit_in_mesa:
            labels = ['CI']
        else:
            labels = []
        merge_request = fork_project.mergerequests.create(
            {'source_branch': branch,
             'target_branch': target_project.default_branch,
             'target_project_id': target_project.id,
             'title': mr_title,
             'labels': labels})
        print(f"Created merge request {merge_request.web_url}")
    else:
        print(f"Pushed to existing merge request {merge_request.web_url}")
    return merge_request


def __append_note_to_merge_request(
        merge_request: ProjectMergeRequest,
        comments: Tuple[list, list, list]
        ) -> None:
    """
    With new or in-progress merge requests, add a note with information about
    the current proposal to uprev.
    :param merge_request: ProjectMergeRequest
    :param comments: list
    :return:
    """
    comments = ["".join(c) for c in comments if c]
    merge_request.notes.create({"body": "\n".join(comments)})


def __run_merge_request_pipeline(
        commit_hash: str
        ) -> None:
    """
    Once the merge request has the information about the uprev proposal, a
    pipeline is created to verify that the proposal doesn't brake previous work.
    Creating, triggering, and monitoring the evolution is the last part of the
    tasks of this tool.
    :param commit_hash: str
    :return: None
    """
    global working_project
    working_project = target_project
    merge_request_pipeline = __wait_pipeline_creation(
        commit_hash, source='merge_request_event')
    failures = run(merge_request_pipeline)
    if failures:
        raise AssertionError(f"Something wrong in the final pipeline "
                             f"{merge_request_pipeline.web_url}")


def __source_comment(
        pipeline: ProjectPipeline
        ) -> list:
    """
    With environment information, prepare a few lines for the merge request
    note reporting how this proposal has been produced.
    :param pipeline: ProjectPipeline
    :return: lines as list
    """
    job_id = os.getenv('CI_JOB_ID')
    if job_id is not None:
        uprev_project = gl.projects.get(os.getenv('CI_PROJECT_ID'))
        uprev_job = uprev_project.jobs.get(job_id)
        source_comment = [
            f"This information comes from the execution of ci-uprev in job "
            f"[{uprev_job.id}]({uprev_job.web_url}).\n"]
    else:
        source_comment = [
            f"This information comes from a local execution of ci-uprev.\n"]
    pipelines_summary = f"Related pipeline: " \
                        f"[{pipeline.id}]({pipeline.web_url})"
    pipelines_summary += ".\n"
    source_comment += [pipelines_summary]
    return source_comment


def __generate_comment(
        by_category: dict,
        as_list: bool
        ) -> list:
    """
    Transform the dictionary with the information by category to the format we
    place this report in the merge request note.
    :param by_category: dict
    :param as_list: bool
    :return: lines as list
    """
    comment = []
    for category, problem in by_category.items():
        category_comment = [category]
        for test_name, result in problem.items():
            if as_list:
                line = f"- {test_name}, {list(result)}\n"
            else:
                line = f"- {test_name}, {result}\n"
            category_comment.append(line)
        category_comment = __truncate_long_messages(category_comment)
        category_comment.append("\n")
        comment.extend(category_comment)
    return comment


def __get_merge_request_title(
        expectations_updated: bool
        ) -> str:
    """
    The title of the merge request depends on certain variables. Based on the
    value they have, the merge request title is built.
    :param expectations_updated: bool
    :return: merge request title
    """
    if IN_PRODUCTION:
        mr_title = MR_TITLE
    else:
        mr_title = f'[TESTONLY] {MR_TITLE}'
    if expectations_updated:
        mr_title = f"{mr_title} {EXPECTATIONS_UPDATED_SUFFIX}"
        print(f"Possible regressions found, "
              f"modifying the MR title to {mr_title!r}")
    return mr_title


def __truncate_long_messages(
        messages: list[str]
        ) -> list[str]:
    """
    When a message list of lines is too long and could disturb the reading of
    the user, this method packs the lines over a limit in a details Markdown
    code.
    :param messages: lines in a message
    :return: original or, if needed, truncated message
    """
    if len(messages) > LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE:
        messages, details, excedent = \
            messages[:LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE], \
            messages[LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE:MAX_LINES_PER_CATEGORY_IN_NOTE], \
            messages[MAX_LINES_PER_CATEGORY_IN_NOTE:]
        messages.append("\n\n<details>\n")
        messages.append("\n<summary>... and more</summary>\n\n")
        messages.extend(details)
        if excedent:
            # TODO: link to the commit content
            messages.append(
                "\nToo much, to see more check the file in the commit\n")
        messages.append("\n</details>\n")
    return messages


def main():
    args = __cli_arguments()
    prepare_environment()

    if args.subcommand == 'collate':
        __cmd_collate_results(args.PIPELINE_ID)
    elif args.subcommand == 'retry':
        __cmd_retry_pipeline(args.PIPELINE_ID)
    elif args.subcommand == 'continue':
        __cmd_continue_uprev(args.PIPELINE_ID)
    else:
        __cmd_uprev(args.revision, args.pipeline)


def __cli_arguments() -> Namespace:
    """
    Define the command line behavior for the arguments and subcommands.
    :return: argument parser
    """
    parser = ArgumentParser(
        description="CLI tool to update the revision of a project used "
                    "inside another project.")
    # TODO: mention that no argument is required
    #  all the configuration elements come from environment variables and this
    #  arguments are only for development and debugging.
    #  So, also show help about those envvars
    #   TARGET_PROJECT_PATH, FORK_PROJECT_PATH, DEP_PROJECT_PATH, GITLAB_TOKEN
    parser.add_argument("--revision", metavar="HASH",
                        help="Specify the revision to uprev.")
    parser.add_argument("--pipeline", metavar="PIPELINE_ID",
                        help="Specify the pipeline to reference in the uprev. "
                             "Requires to have specified the revision.")
    subparsers = parser.add_subparsers(title='subcommands', dest='subcommand',
                                       description='valid subcommands',
                                       help='sub-command additional help')
    parser_collateresults = subparsers.add_parser(
        'collate', help="Collate results from an existing pipeline.")
    parser_collateresults.add_argument(
        "PIPELINE_ID", help="Only collate results of the pipeline.")
    parser_retrypipeline = subparsers.add_parser(
        'retry', help="Retry an existing pipeline.")
    parser_retrypipeline.add_argument(
        "PIPELINE_ID", help="Collate results and then retry failed jobs "
                            "and collate again.")
    parser_continue = subparsers.add_parser(
        'continue', help="Specify a point in the process to continue from")
    parser_continue.add_argument(
        "PIPELINE_ID", help="Continue like if the execution was working with "
                            "this pipeline")
    return parser.parse_args()


def __cmd_collate_results(
        pipeline_id: int
        ) -> None:
    """
    Get the failures and report about them
    :param pipeline_id: identification number
    :return:
    """
    pipeline = working_project.pipelines.get(pipeline_id)
    print(f"Only collate results from pipeline {pipeline_id} "
          f"in {working_project.path_with_namespace}.")
    failures, _, jobs_without_artifacts = collate_results(pipeline)
    if jobs_without_artifacts:
        job_names = [job.name for job in jobs_without_artifacts]
        print(f"These jobs failed to produce artifacts: "
              f"{', '.join(job_names)}")
    if failures:
        print(f"Failures:\n{pformat(failures)}")


def __cmd_retry_pipeline(
        pipeline_id: int
        ) -> None:
    """
    Direct access to the pipeline post-process. When there is a problem with
    how a pipeline has finished and/or how the ci-uprev did to post-process,
    this is the option to review under similar conditions.
    :param pipeline_id: identification number
    :return:
    """
    pipeline = working_project.pipelines.get(pipeline_id)
    print(f"Collate current results of the pipeline {pipeline_id} and retry"
          f"in {working_project.path_with_namespace}.")
    failures = __post_process_pipeline(pipeline)
    if failures:
        print(f"Failures:\n{pformat(failures)}")


def __cmd_continue_uprev(
        pipeline_id: int
        ) -> None:
    """
    This is a debug purpose option. When an uprev didn't finish as expected,
    instead of fix and relaunch from scratch, this option can cling to the
    pipeline of a previous uprev and proceed like if it did it from the
    beginning.
    :param pipeline_id:
    :return:
    """
    repo = git.Repo(LOCAL_CLONE)
    pipeline = working_project.pipelines.get(pipeline_id)
    failures = __post_process_pipeline(pipeline)
    update_branch(repo, failures)
    push_to_merge_request(repo, failures, pipeline)


def __cmd_uprev(
        revision: str,
        pipeline_id: int,
        ) -> None:
    """
    This is the default procedure of the uprev tool. Without options, the
    default, it will search for a pipeline in the project to uprev that
    satisfies the necessary conditions. The commit of this pipeline will be used
    to uprev in the target project. (Those two variables, revision and pipeline,
    can be fixed using option arguments for debug purposes like repeat and
    compare with a previous execution.)

    Then the uprev candidate is tested, if there are expectations to update
    they are and the commit amended, to then generate a merge request proposal.

    :param revision: optional parameter to specify the revision to use
    :param pipeline_id: optional parameter to specify the pipeline to use
    :return:
    """
    if not revision and not pipeline_id:
        pipeline_id, revision = get_candidate_for_uprev()
        print(f"Found pipeline {pipeline_id} with SHA {revision} "
              f"{dep_project.commits.get(revision).web_url}")
    else:
        if revision:
            pipeline_id = __search_pipeline(revision)
            print(f"Specified the revision to uprev to {revision} and found "
                  f"the pipeline {pipeline_id} "
                  f"{dep_project.commits.get(revision).web_url}")
        elif pipeline_id:
            revision = __get_pipeline_commit(pipeline_id)
            print(f"Specified the pipeline to uprev to {pipeline_id} and found "
                  f"the revision {revision} "
                  f"{dep_project.commits.get(revision).web_url}")
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        templates_commit = get_templates_commit(revision)
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        templates_commit = get_templates_commit("main")
    else:
        raise NotImplementedError(f"Not yet implemented the uprev of "
                                  f"{uprev_pair.name}")
    repo = create_branch(pipeline_id, revision, templates_commit,
                         target_project.default_branch)
    pipeline = push(repo)
    failures = run(pipeline)
    update_branch(repo, failures)
    push_to_merge_request(repo, failures, pipeline)


def __search_pipeline(
        revision: str
        ) -> int:
    """
    When a revision is specified, the pipeline it has generated is needed.
    :param revision: dep project revision
    :return: pipeline id
    """
    pipelines = dep_project.pipelines.list(sha=revision, state='success')
    if not pipelines:
        raise ValueError(f"No pipeline found with this revision")
    return pipelines[-1].id


def __get_pipeline_commit(
        pipeline_id: int
        ) -> str:
    """
    When a pipeline is specified, the revision that generated it is needed.
    :param pipeline_id: identification number
    :return: string with the commit sha
    """
    pipeline = dep_project.pipelines.get(pipeline_id)
    return pipeline.sha


if __name__ == '__main__':
    main()
